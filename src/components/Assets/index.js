const IMAGES = {
  RpsBatu: require("./batu.png"),
  RpsKertas: require("./kertas.png"),
  RpsGunting: require("./gunting.png"),
  BackButton: require("./back.png"),
  RpsLogo: require("./logo.png"),
  RpsRefresh: require("./refresh.png"),
  EmblemLogo: require("./emblem.svg"),
};

export default IMAGES;
